This is the frontend part of the risk type application. On the main page (/main page) is a list of all available types and button for adding new risk types (/new_risk_type page). When existed risk type is clicked, you can edit or delete it (/risk_type_details/:rist_type_id/ page).
AngularJS v-1.5.3 is used as a framework for the application. The application was build by gulp v-3.9.0.
