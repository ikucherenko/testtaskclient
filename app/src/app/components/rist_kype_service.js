(function () {
    'use strict';


    angular.module('app')
    .factory("riskTypeService", function ($http, utilsService) {
        function getRiskTypes() {
            return $http.get(utilsService.API + 'risk_type')
                .then(function(response) {
                    return response;
                },function(error) {
                    return error;
               });
        }

        function getRiskTypeById(id) {
            return $http.get(utilsService.API + 'risk_type/'+id)
            .then(function(response) {
                return response;
            }, function(error) {
                return error;
            });
        }

        function editRiskType(riskType) {
            return $http.put(utilsService.API + 'risk_type/' + riskType.id, riskType)
            .then(function(response) {
                return response;
            }, function(error) {
                return error;
            });
        }

        function delRiskType(id) {
            return $http.delete(utilsService.API + 'risk_type/'+id)
            .then(function(response) {
                return response;
            }, function(error) {
                return error;
            })
        }

        function addRiskType(riskType) {
            return $http.post(utilsService.API + 'risk_type', riskType)
            .then(function(response) {
                return response;
            }, function(error) {
                return error;
            });
        }

        return {
            getRiskTypes: getRiskTypes,
            getRiskTypeById: getRiskTypeById,
            addRiskType: addRiskType,
            editRiskType: editRiskType,
            delRiskType: delRiskType
        };

    })
})();