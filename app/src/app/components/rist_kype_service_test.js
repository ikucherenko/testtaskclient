
describe('Service: riskTypeService', function() {
  beforeEach(module('app'));

  beforeEach(angular.mock.inject(function($httpBackend, $rootScope, $http, riskTypeService) {
    this.$scope = $rootScope.$new();
    this.$httpBackend = $httpBackend;
    this.$http = $http;
    this.riskTypeService = riskTypeService;
  }));


   it('should return list of risk types', function() {
    var result;

    this.$httpBackend.expectGET('http://localhost:8000/risk_type').respond([{id:1}, {id:2}]);
    this.riskTypeService.getRiskTypes().then(function(data) {
      result = data;
    });
    this.$httpBackend.flush();
    expect(result.data).toEqual([{id:1}, {id:2}]);
  });


  it('should return risk type by id', function() {
    var result;

    this.$httpBackend.expectGET('http://localhost:8000/risk_type/1').respond({id:1, name: 'test'});
    this.riskTypeService.getRiskTypeById(1).then(function(data) {
      result = data;
    });
    this.$httpBackend.flush();
    expect(result.data).toEqual({id:1, name: 'test'});
  });

  it('should change risk type', function() {
    var result;

    this.$httpBackend.expectPUT('http://localhost:8000/risk_type/1').respond({id:1, name: 'test'});
    this.riskTypeService.editRiskType({id:1, name: 'test'}).then(function(data) {
      result = data;
    });
    this.$httpBackend.flush();
    expect(result.data).toEqual({id:1, name: 'test'});
  });


    it('should create new risk type', function() {
    var result;

    this.$httpBackend.expectPOST('http://localhost:8000/risk_type').respond({id:1, name: 'test'});
    this.riskTypeService.addRiskType({name: 'test'}).then(function(data) {
      result = data;
    });
    this.$httpBackend.flush();
    expect(result.data).toEqual({id:1, name: 'test'});
  });

  it('should change risk type', function() {
    var result;

    this.$httpBackend.expectDELETE('http://localhost:8000/risk_type/1').respond({'test': 'test'});
    this.riskTypeService.delRiskType(1).then(function(data) {
      result = data;
    });
    this.$httpBackend.flush();
    expect(result.data).toEqual({'test': 'test'});
  });




});