describe('Directive: riskTypeFields', function () {
  'use strict';

  beforeEach(module('app'));
  beforeEach(angular.mock.inject(function ($compile, $rootScope) {
    this.$compile = $compile;
    this.$scope = $rootScope.$new();

    this.type = {name: "test"};

    this.$scope.type = this.type;



    this.createDirective = function () {
      this.element = angular.element('<risk-type-fields type="risk_type"></risk-type-fields>');
      this.el = this.$compile(this.element)(this.$scope);
      this.$scope.$digest();
      this.vm = this.el.controller('riskTypeFields');
    }
  }));

  afterEach(function () {
    //prevents DOM elements leak
    if (this.el) {
      this.el.remove();
      this.el = null;
    }
  });

  // it('Should be initializated correct', function () {
  //   this.createDirective();
  //   // console.log(this.$scope);
  //   expect(this.$scope.newItem).toBeDefined();
  // });


});