(function() {
  'use strict';

  angular
    .module('app')
    .controller('RiskTypeDetailsController', RiskTypeDetailsController);

  /** @ngInject */
  function RiskTypeDetailsController(riskTypeService, $log, $routeParams, $location) {
    var vm = this;
    vm.typeId = $routeParams.id || null;
    vm.title = '';
    vm.error = '';


    vm.del = del;
    vm.save = save;
    vm.close = close;


    activate();

    function activate() {
        if (vm.typeId !== null) {
            riskTypeService.getRiskTypeById(vm.typeId)
            .then(function(response) {
                vm.currentRiskType = response.data;
                vm.title = 'Risk type '+ vm.currentRiskType.name + ' details';
                }, function(error) {
                $log.error(error);
            });
        }
        else {
            vm.title = 'Add new type of risk';
            vm.currentRiskType = {
                field_set : [],
                name : '',
                description : ''
            }
        }
    }

    function del() {
        riskTypeService.delRiskType(vm.currentRiskType.id)
        .then(function(response) {
            $location.path('/main');
        }, function(error) {
            $log.error(error);
            vm.error = error;
        })
    }

    function save() {
        if (vm.typeId == null && vm.currentRiskType.id == undefined) {
            riskTypeService.addRiskType(vm.currentRiskType)
            .then(function(response) {
                vm.currentRiskType = response.data;
                vm.title = 'Risk type '+ vm.currentRiskType.name + ' details';
            }, function(error) {
                $log.error(error);
            });
        } else {
            riskTypeService.editRiskType(vm.currentRiskType)
            .then(function(response) {
                vm.currentRiskType = response.data;
                vm.title = 'Risk type '+ vm.currentRiskType.name + ' details';
                $location.path('/main');
            }, function(error) {
                $log.error(error);
            });
        }
    }

     function close() {
        $location.path('/main');
    }

  }


  })();
