(function () {
  'use strict';

  angular
    .module('app')
    .directive('riskTypeFields', riskTypeFields);

  function riskTypeFields() {
    return {
      scope: {
        type: '='
      },
      restrict: 'EA',
      templateUrl: 'app/risk_type_details/risk_type_fields_directive.html',
      controller: RiskTypeFieldsController,
      controllerAs: 'vm'
    };

  }

    /* @ngInject */
  function RiskTypeFieldsController($scope) {

    $scope.newField = {
      field_type: {id: null},
      title: null,
      enum_set: []
    };

    $scope.newItem = null;

    $scope.fieldTypes = [
      {id: 1, name: 'Text'},
      {id: 2, name: 'Number'},
      {id: 3, name: 'Date'},
      {id: 4, name: 'Enum'}
    ];


    $scope.addField = function() {
      $scope.type.field_set.push(angular.copy($scope.newField));
    };

    $scope.delField = function(field) {

      var index = $scope.type.field_set.indexOf(field);
        if (index > -1) {
          $scope.type.field_set.splice(index, 1);
        }
    };

    $scope.addItem = function(newItem) {

      $scope.newField.enum_set.push({name: newItem});
    };  
  }


})();