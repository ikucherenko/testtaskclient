describe('Controller: RiskTypeDetailsController', function() {
  'use strict';


    beforeEach(module('app'));

    beforeEach(angular.mock.inject(function (_riskTypeService_, _$httpBackend_, $controller, $log, $q, _$rootScope_, $location) {
      this.$httpBackend = _$httpBackend_;
      this.scope = _$rootScope_.$new();
      this.riskTypeService = _riskTypeService_;
      this.log = $log;
      this.q = $q;
      this.location = $location;
      this.risTypeDefer = this.q.defer();
      this.fakeParams = {}

      spyOn(this.riskTypeService, 'getRiskTypeById').and.returnValue(this.risTypeDefer.promise);
      spyOn(this.riskTypeService, 'delRiskType').and.returnValue(this.risTypeDefer.promise);
      spyOn(this.riskTypeService, 'addRiskType').and.returnValue(this.risTypeDefer.promise);
      spyOn(this.riskTypeService, 'editRiskType').and.returnValue(this.risTypeDefer.promise);


      this.vm = $controller('RiskTypeDetailsController', {
      riskTypeService: this.riskTypeService,
      $log: this.log,
      $routeParams: this.fakeParams,
      $scope: this.scope,
      $location: this.location
    });
  }));

describe('initialization', function(){

  it('should get data from server if exist type of risk is selected', function() {
    this.risTypeDefer.resolve({data: {test: 'test'}});
    this.scope.$apply();
    expect(this.vm.currentRiskType).toBeDefined();
  });


  it('should create new empty type of risk', function() {
      expect(this.vm.currentRiskType.field_set).toEqual([]);
    });
  });


describe('del function', function() {
  it('should delete selected type of risk', function() {
    this.vm.currentRiskType.id = 1;
    spyOn(this.location, 'path').and.callThrough();
    this.risTypeDefer.resolve();
    this.vm.del();
    this.scope.$apply();

    expect(this.location.path).toHaveBeenCalledWith('/main');
  });
});


describe('save function', function() {
  it('should save new model of risk type', function() {
    this.vm.typeId = null
    this.vm.currentRiskType = {};
    this.risTypeDefer.resolve({data: {test: 'test', name: 'name'}});
    this.vm.save();
    this.scope.$apply();
    expect(this.vm.currentRiskType).toEqual({test: 'test', name: 'name'});
  });

  it('should change exist model of risk type', function() {
    this.vm.typeId = 1;
    spyOn(this.location, 'path').and.callThrough();
    this.risTypeDefer.resolve({data: {test: 'test', name: 'name'}});
    this.vm.save();
    this.scope.$apply();
    expect(this.location.path).toHaveBeenCalledWith('/main');
  });
});

describe('close function', function() {
  it('should redirect to main page', function() {
    spyOn(this.location, 'path').and.callThrough();
    this.vm.close();
    expect(this.location.path).toHaveBeenCalledWith('/main');
  });
});


});