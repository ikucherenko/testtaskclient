describe('Controller: MainController', function() {
  'use strict';


    var respond = {'test1': 'test1'};

  beforeEach(module('app'));

    beforeEach(angular.mock.inject(function (_riskTypeService_, _$httpBackend_, $controller, $log, $q, _$rootScope_) {
      this.$httpBackend = _$httpBackend_;
      this.scope = _$rootScope_.$new();
      this.riskTypeService = _riskTypeService_;
      this.log = $log;
      this.q = $q;
      this.$httpBackend.whenGET('http://localhost:8000/risk_type').respond(respond);

    this.vm = $controller('MainController', {
      riskTypeService: this.riskTypeService,
      $log: this.log
    });
  }));

describe('initialization', function(){

  it('should get data from server when start', function() {
    this.$httpBackend.flush();
    expect(this.vm.allRisks).toBeDefined();
  });
});


});