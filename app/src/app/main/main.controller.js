(function() {
  'use strict';

  angular
    .module('app')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(riskTypeService, $log) {
    var vm = this;

    activate();

    function activate() {

      riskTypeService.getRiskTypes()
      .then(function(response) {
        vm.allRisks = response.data;
      }, function(error) {
        $log.error(error);
      });
    }
  }
})();
