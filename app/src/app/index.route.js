(function() {
  'use strict';

  angular
    .module('app')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/main', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .when(
        '/risk_type_details/:id/', {
        templateUrl: 'app/risk_type_details/risk_type_details.html',
        controller: 'RiskTypeDetailsController',
        controllerAs: 'vm'
        })
      .when('/new_risk_type', {
        templateUrl: 'app/risk_type_details/risk_type_details.html',
        controller: 'RiskTypeDetailsController',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/main'
      });
  }

})();
